﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSystem_Manager : MonoBehaviour
{
    public static CharacterSystem_Manager Instance;

    public Character[] characters;
    public Monster monster;
    public Monster[] monsterArr;

    public Healer healer;
    
    private int HeroCount;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        HeroCount = 4;
    }

    public void GameStart()
    {
        for (int i = 0; i < characters.Length; i++)
        {
            characters[i].GameStart();
        }

        int _stageID = GameSystem_Manager.Instance.StageID;
        monster = monsterArr[_stageID - 1];
        monster.gameObject.SetActive(true);
    }

    public void HeroDie()
    {
        HeroCount -= 1;

        if (HeroCount == 0)
        {
            HeroCount = 4;
        }
    }
}
