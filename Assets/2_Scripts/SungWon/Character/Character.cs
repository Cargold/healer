﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold.WhichOne;
using System;

public class AI
{
    Character character;
    public AI(Character _character)
    {
        character = _character;
    }

    public bool CanSkill(Skill_Script _skil)
    {
        float ManaCost;

        if (_skil != null)
        {
            ManaCost = _skil.ManaCost;

            if (character.CurrentMP >= ManaCost)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}

public abstract class Character : Character_Cargold
{
    public enum CharacterState
    {
        Idle,
        Skill,
        Dead
    }

    public float MaxHP;
    [HideInInspector]
    public float CurrentHP;
    public float MaxMP;
    [HideInInspector]
    public float CurrentMP;
    public float ManaRegenCost;
    private float CurrentDef;

    protected CharacterState CurrentState;

    protected AI ai;
    public Animator animator;

    public Skill_Script[] skils;

    public Character_View_Script viewClass;
    
    void OnEnable()
    {
        CurrentHP = MaxHP;
        CurrentMP = MaxMP;
        CurrentState = CharacterState.Idle;
        ai = new AI(this);

        viewClass = this.GetComponent<Character_View_Script>();
        viewClass.Init_Func();

        this.skils = GetInitSkill_Func();

        if(this.skils != null)
        {
            for (int i = 0; i < this.skils.Length; i++)
            {
                this.skils[i].Init_Func(this);
            }
        }
        else
        {
            
        }
    }
    protected virtual void Init_Func()
    {

    }

    protected abstract Skill_Script[] GetInitSkill_Func();
   
    protected IEnumerator ManaRegen_Cor()
    {
        while (this.CurrentState != CharacterState.Dead)
        {
            yield return Cargold_Library.GetWaitForSeconds_Func(1f);

            this.CurrentMP += ManaRegenCost;
            UseSkill_Func();
        }
    }

    protected virtual void UseSkill_Func()
    {
        for (int i = 0; i < this.skils.Length; i++)
        {
            Skill_Script _skillClass = skils[i];

            if (_skillClass.IsCoolTiming == false)
            {
                this.CoolTimeDone_Func(_skillClass);
            }
            else
            {

            }
        }
    }

    public void SelectedCharacter()
    {
        //플레이어매니저에게 선택됨을 알려주는 내용
    }

    void TakeDamage(float Damage)
    {
        CurrentHP -= Damage;

        if (CurrentHP <= 0)
        {
            this.Die_Func();
        }
        else
        {
            viewClass.Damaged_Func();
        }
    }

    protected virtual void Die_Func()
    {
        CurrentHP = 0;
        CurrentState = CharacterState.Dead;

        viewClass.Die_Func();
    }

    protected virtual bool HPCheck()
    {
        return false;
    }

    float GetHP()
    {
        return CurrentHP;
    }

    float GetMP()
    {
        return CurrentMP;
    }

    public override bool HasMana_Func(float _mana)
    {
        //매개변수 값에 해당하는 마나가 있는지 확인 있으면 true
        //없으면 false

        if (CurrentMP > _mana)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public override void UseSkill_Func(float _mana)
    {
        //매개변수 값에 해당하는 마나를 현재마나에서 빼준다.
        CurrentMP -= _mana;

        if (CurrentMP < 0)
        {
            CurrentMP = 0;
        }

        //스킬에 해당하는 애니메이션 실행
        CurrentState = CharacterState.Skill;
    }

    //선택된 대상이 취소되면 호출
    public override void SelectCancel_Func()
    {

    }

    //선택이 최종적으로 되엇으면 아래의 함수 호출
    public override void Selected_Func(bool _repeat = false)
    {

    }

    //쿨타임이 다 되었다고 알려주는 함수
    public override void CoolTimeDone_Func(Skill_Script _skils)
    {
        if (ai.CanSkill(_skils))
        {
            _skils.UseSkill_Func();
        }
    }

    //데미지 받앗다 호출
    public override void Damaged_Func(float _damageValue)
    {
        TakeDamage(_damageValue);
    }

    //방어력이 지금 현재 몇인지
    public override float GetDefence_Func()
    {
        return CurrentDef;
    }

    //방어력 설정해주는 함수
    public override void SetDefence_Func(float _setDefenceValue)
    {
        CurrentDef += _setDefenceValue;
    }

    public virtual void GameStart()
    {
        StartCoroutine(ManaRegen_Cor());

        for (int i = 0; i < this.skils.Length; i++)
        {
            skils[i].GameStart_Func();
        }
    }

    public override void Heal_Func(float _healValue)
    {
        
    }
}
