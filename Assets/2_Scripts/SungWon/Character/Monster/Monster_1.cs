﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster_1 : Monster
{
    protected override Skill_Script[] GetInitSkill_Func()
    {
        return new Skill_Script[]
        {
            DataBase_Manager.Instance.GetSkillClass_Func(SkillType.Monster_First_NormalAttack),
            DataBase_Manager.Instance.GetSkillClass_Func(SkillType.Monster_First_NormalAttack2),
            DataBase_Manager.Instance.GetSkillClass_Func(SkillType.Monster_First_WholeAttack),
        };
    }

    protected override int GetMonsterID_Func()
    {
        return 0;
    }
}
