﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster_3 : Monster
{
    protected override Skill_Script[] GetInitSkill_Func()
    {
        return new Skill_Script[]
        {
            DataBase_Manager.Instance.GetSkillClass_Func(SkillType.Monster_Third_DeathRing),
            DataBase_Manager.Instance.GetSkillClass_Func(SkillType.Monster_Third_HitAndCurse),
            DataBase_Manager.Instance.GetSkillClass_Func(SkillType.Monster_Third_LastWill),
            DataBase_Manager.Instance.GetSkillClass_Func(SkillType.Monster_Third_ResistByDamage),
        };
    }

    protected override int GetMonsterID_Func()
    {
        return 2;
    }
}
