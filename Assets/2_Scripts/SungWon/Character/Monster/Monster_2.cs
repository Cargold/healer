﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster_2 : Monster
{
    protected override Skill_Script[] GetInitSkill_Func()
    {
        return new Skill_Script[]
        {
            DataBase_Manager.Instance.GetSkillClass_Func(SkillType.Monster_Second_NormalAttack),
            DataBase_Manager.Instance.GetSkillClass_Func(SkillType.Monster_Second_Poison),
            DataBase_Manager.Instance.GetSkillClass_Func(SkillType.Monster_Second_Silence),
        };
    }

    protected override int GetMonsterID_Func()
    {
        return 1;
    }
}
