﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Monster : Character
{
    public int monsterID;

    protected override void Init_Func()
    {
        base.Init_Func();

        monsterID = GetMonsterID_Func();
    }

    protected abstract int GetMonsterID_Func();

    protected override void Die_Func()
    {
        base.Die_Func();

        StageSystem_Manager.Instance.MonsterDie_Func();
    }
}
