﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character_View_Script : MonoBehaviour
{
    public SpriteRenderer spriteRend;
    public Animator animator;

    public void Init_Func()
    {

    }

    public void Damaged_Func()
    {
        StartCoroutine(Damaged_Cor());
    }

    IEnumerator Damaged_Cor()
    {
        spriteRend.color = Color.red;

        yield return Cargold_Library.GetWaitForSeconds_Func(0.5f);

        spriteRend.color = Color.white;
    }

    public void Die_Func()
    {
        GameObject.Destroy(this.gameObject);
    }
}
