﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warrior : Character
{
    protected override Skill_Script[] GetInitSkill_Func()
    {
        Skill_Script _skillClass = DataBase_Manager.Instance.GetSkillClass_Func(SkillType.Warrior_NormalAttack);
        return new Skill_Script[] { _skillClass };
    }

    protected override void Die_Func()
    {
        base.Die_Func();

        CharacterSystem_Manager.Instance.HeroDie();
    }
}
