﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healer : Character
{
    protected override Skill_Script[] GetInitSkill_Func()
    {
        return null;
    }

    protected override void UseSkill_Func()
    {
        
    }

    public void SetCurrentSkil(Skill_Script[] NewSkill)
    {
        skils = NewSkill;    
    }

    protected override void Die_Func()
    {
        base.Die_Func();

        CharacterSystem_Manager.Instance.HeroDie();
    }

    public override void GameStart()
    {
        base.skils = PlayerSystem_Manager.Instance.EquipSkillClassArr;

        StartCoroutine(base.ManaRegen_Cor());
    }
}
