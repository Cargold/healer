﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thief : Character
{
    private bool InvincibilityFlag=false;

    protected override Skill_Script[] GetInitSkill_Func()
    {
        Skill_Script _skillClass = DataBase_Manager.Instance.GetSkillClass_Func(SkillType.Thief_NormalAttack);
        return new Skill_Script[] { _skillClass };
    }

    protected override void Die_Func()
    {
        base.Die_Func();

        CharacterSystem_Manager.Instance.HeroDie();
    }

    protected override bool HPCheck()
    {
        float HPPercent = CurrentHP / MaxHP;

        if(HPPercent < 0.2f)
        {
            StartCoroutine(InvincibilityCoroutine());
            return true;
        }
        else
        {
            return false;
        }
    }

    IEnumerator InvincibilityCoroutine()
    {
        InvincibilityFlag = true;

        yield return Cargold_Library.GetWaitForSeconds_Func(5f);

        InvincibilityFlag = false;
    }

    public override void Damaged_Func(float _damageValue)
    {
        if(!InvincibilityFlag)
        {
            base.Damaged_Func(_damageValue);
        }
    }
}
