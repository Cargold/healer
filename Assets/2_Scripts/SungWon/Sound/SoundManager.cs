﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sound
{
    public string Name;
    public AudioClip Clip;

    private AudioSource source;

    [Range(0f, 10f)]
    public float Volume;
    [Range(0.5f, 1.5f)]
    public float Pitch;

    public void SetSource(AudioSource Source)
    {
        source = Source;
        source.clip = Clip;
    }

    public void Play(bool LoopFlag, float Rate)
    {
        if (LoopFlag)
        {
            source.loop = true;
        }
        else
        {
            source.loop = false;
        }

        source.time = Rate;
        source.volume = Volume;
        source.pitch = Pitch;
        source.Play();
    }
}

public class SoundManager : Cargold_Singleton<SoundManager> {

    [SerializeField]
    Sound[] Sounds;

    void Start ()
    {
        for (int i = 0; i < Sounds.Length; i++)
        {
            GameObject NewSound = new GameObject("NewSound_" + i + "_" + Sounds[i].Name);
            NewSound.transform.SetParent(this.transform);
            Sounds[i].SetSource(NewSound.AddComponent<AudioSource>());
        }
    }

    public void PlaySound(string _Name, bool _LoopFlag, float _Rate)
    {
        for (int i = 0; i < Sounds.Length; i++)
        {
            if (Sounds[i].Name == _Name)
            {
                Sounds[i].Play(_LoopFlag, _Rate);
                return;
            }
        }
    }

    public override IEnumerator Init_Cor()
    {
        yield break;
    }
}
