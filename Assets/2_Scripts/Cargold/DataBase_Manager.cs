﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataBase_Manager : Cargold_Singleton<DataBase_Manager>
{
    private Dictionary<SkillType, Skill_Script> skillClassDic;

    public void Init_Func()
    {
        skillClassDic = new Dictionary<SkillType, Skill_Script>();

        GameObject[] _skillObjArr = Resources.LoadAll<GameObject>("Skill");

        Transform _thisTrf = this.transform;

        for (int i = 0; i < _skillObjArr.Length; i++)
        {
            string _skillName = _skillObjArr[i].name;

            GameObject _skillObj = GameObject.Instantiate(_skillObjArr[i], _thisTrf);
            Skill_Script _skillClass = _skillObj.GetComponent<Skill_Script>();
            SkillType _skillType = _skillName.ToEnum<SkillType>();

            skillClassDic.Add_Func(_skillType, _skillClass);
        }
    }

    public Skill_Script GetSkillClass_Func(SkillType _skillType)
    {
        Skill_Script _skillClass = skillClassDic.GetValue_Func(_skillType);
        
        return _skillClass;
    }

    public override IEnumerator Init_Cor()
    {
        yield break;
    }
}