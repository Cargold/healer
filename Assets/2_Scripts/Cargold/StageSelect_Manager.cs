﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageSelect_Manager : MonoBehaviour
{
    public static StageSelect_Manager Instance;

    public GameObject stageSelectObj;

    public GameObject LoadingObj;

    public void Init_Func()
    {
        Instance = this;

        LoadingObj.SetActive(false);
    }

    public void CallBtn_StageSelect_Func(int _stageID)
    {
        GameSystem_Manager.Instance.StageSelect_Func(_stageID);

        LoadingObj.SetActive(true);
    }
}
