﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold.WhichOne;

public class PlayerSystem_Manager : Cargold_Singleton<PlayerSystem_Manager>
{
    private Skill_Script[] equipSkillClassArr;
    private Skill_Script[] reserveSkillClassArr;
    
    public Skill_Script[] EquipSkillClassArr { get { return equipSkillClassArr; } }
    public Skill_Script[] ReserveSkillClassArr { get { return reserveSkillClassArr; } }

    public void Init_Func()
    {
        equipSkillClassArr = null;
    }
    
    public void SetHealerInitSkill_Func()
    {
        equipSkillClassArr = new Skill_Script[]
            {
                DataBase_Manager.instance.GetSkillClass_Func(SkillType.Healer_ImmediateHeal),
                DataBase_Manager.instance.GetSkillClass_Func(SkillType.Healer_RepeatHeal),
            };
    }

    public void SetSkill_Equip_Func(Skill_Script[] _skillClassArr)
    {
        equipSkillClassArr = _skillClassArr;
    }

    public void SetSkill_Reserve_Func(Skill_Script[] _skillClassArr)
    {
        reserveSkillClassArr = _skillClassArr;
    }

    public override IEnumerator Init_Cor()
    {
        yield break;
    }
}

public abstract class Character_Cargold : MonoBehaviour, IWhichOne
{
    public abstract void CoolTimeDone_Func(Skill_Script _skillClass);
    public abstract bool HasMana_Func(float _mana);
    public abstract void UseSkill_Func(float _mana);

    public abstract void SelectCancel_Func();
    public abstract void Selected_Func(bool _repeat = false);

    public abstract void Damaged_Func(float _damageValue);
    public abstract void Heal_Func(float _healValue);
    public abstract float GetDefence_Func();
    public abstract void SetDefence_Func(float _setDefenceValue);
}

public enum SelectType
{
    None = -1,
    Skill,
    Character,
}