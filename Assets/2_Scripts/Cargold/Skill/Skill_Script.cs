﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold.WhichOne;

public abstract class Skill_Script : MonoBehaviour
{
    [SerializeField] private Character_Cargold charClass;
    [SerializeField] private Sprite skillIconSprite;
    [SerializeField] private string skillName;
    [SerializeField] private string skillDesc;
    [SerializeField] private AudioClip sfxClip;

    [SerializeField] private float manaCost;
    [SerializeField] private float coolTime;
    private bool isCoolTiming;
    [SerializeField] private float castingTime;

    public SkillType SkillType { get { return this.GetSkillType_Func(); } }
    public Sprite SkillIconSprite { get { return skillIconSprite; } }
    public string SkillName { get { return skillName; } }
    public virtual string SkillDesc { get { return skillDesc; } }
    public AudioClip SfxClip { get { return sfxClip; } }

    public float ManaCost { get { return manaCost; } }
    public float CoolTime { get { return coolTime; } }
    public bool IsCoolTiming { get { return isCoolTiming; } } 
    public float CastingTime { get { return castingTime; } }


    public virtual void Init_Func(Character_Cargold _charClass)
    {
        this.charClass = _charClass;
    }

    protected abstract SkillType GetSkillType_Func(); 

    public virtual void GameStart_Func()
    {
        StartCoroutine(CoolTime_Cor());
    }

    private IEnumerator CoolTime_Cor()
    {
        isCoolTiming = true;

        yield return Cargold_Library.GetWaitForSeconds_Func(this.coolTime);

        isCoolTiming = false;

        charClass.CoolTimeDone_Func(this);
    }

    public virtual void UseSkill_Func()
    {
        if(isCoolTiming == false)
        {
            StartCoroutine(CoolTime_Cor());
        }
        else
        {
            throw new System.Exception("쿨타임 상태에서 스킬 사용이 호출되었습니다. : " + this.SkillType);
        }
    }
}

public enum SkillType
{
    None = -1,

    NormalAttack = 0,

    Warrior_NormalAttack = 100,
    Warrior_Guardian,

    Thief_NormalAttack = 200,
    Thief_StrikeWeakPoint,

    Wizard_NormalAttack = 300,
    Wizard_MagicMissile,

    Healer_ImmediateHeal = 400,
    Healer_RepeatHeal,
    Healer_AreaHeal,
    Healer_Cure,

    Monster_First_NormalAttack = 1000,
    Monster_First_NormalAttack2,
    Monster_First_WholeAttack,

    Monster_Second_NormalAttack = 1100,
    Monster_Second_Poison,
    Monster_Second_Silence,

    Monster_Third_HitAndCurse = 1200,
    Monster_Third_ResistByDamage,
    Monster_Third_DeathRing,
    Monster_Third_LastWill,
}