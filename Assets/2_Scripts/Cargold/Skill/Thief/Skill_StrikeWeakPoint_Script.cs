﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_StrikeWeakPoint_Script : Skill_Script
{
    public override string SkillDesc
    {
        get
        {
            return base.SkillDesc;
        }
    }

    protected override SkillType GetSkillType_Func()
    {
        return SkillType.Thief_StrikeWeakPoint;
    }
}