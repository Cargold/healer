﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalAttack_Thief_Script : NormalAttack_Script
{
    protected override SkillType GetSkillType_Func()
    {
        return SkillType.Thief_NormalAttack;
    }
}