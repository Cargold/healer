﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalAttack_Warrior_Script : NormalAttack_Script
{
    protected override SkillType GetSkillType_Func()
    {
        return SkillType.Warrior_NormalAttack;
    }
}
