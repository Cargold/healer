﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_Guardian_Script : Skill_Script
{
    protected override SkillType GetSkillType_Func()
    {
        return SkillType.Warrior_Guardian;
    }
}