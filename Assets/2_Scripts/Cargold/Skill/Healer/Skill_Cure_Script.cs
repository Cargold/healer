﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_Cure_Script : Skill_Script
{
    public override void UseSkill_Func()
    {
        base.UseSkill_Func();
    }
    protected override SkillType GetSkillType_Func()
    {
        return SkillType.Healer_Cure;
    }
}