﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_AreaHeal_Script : Skill_Script
{
    public float healValue;

    public override string SkillDesc
    {
        get
        {
            return string.Format(base.SkillDesc, healValue);
        }
    }
    public override void UseSkill_Func()
    {
        base.UseSkill_Func();

        Character_Cargold[] _charClassArr = null;

        for (int i = 0; i < _charClassArr.Length; i++)
        {
            _charClassArr[i].Heal_Func(healValue);
        }
    }
    protected override SkillType GetSkillType_Func()
    {
        return SkillType.Healer_AreaHeal;
    }
}