﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_RepeatHeal_Script : Skill_Script
{
    public float lastTime;
    public float healValue;

    public override string SkillDesc
    {
        get
        {
            return string.Format(base.SkillDesc, lastTime, healValue);
        }
    }
    protected override SkillType GetSkillType_Func()
    {
        return SkillType.Healer_RepeatHeal;
    }
    public override void UseSkill_Func()
    {
        base.UseSkill_Func();


    }
}