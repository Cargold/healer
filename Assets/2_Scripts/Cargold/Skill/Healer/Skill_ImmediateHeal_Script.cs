﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_ImmediateHeal_Script : Skill_Script
{
    public float healValue;

    public override void GameStart_Func()
    {
        base.GameStart_Func();

        Debug.LogError("Test");
    }

    public override string SkillDesc
    {
        get
        {
            return string.Format(base.SkillDesc, healValue);
        }
    }
    public override void UseSkill_Func()
    {
        base.UseSkill_Func();
    }
    protected override SkillType GetSkillType_Func()
    {
        return SkillType.Healer_ImmediateHeal;
    }
}