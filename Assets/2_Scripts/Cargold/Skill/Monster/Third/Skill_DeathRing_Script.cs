﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_DeathRing_Script : Skill_Script
{
    public float damageValue;
    public float effectDelayTime;

    public override string SkillDesc
    {
        get
        {
            return string.Format(base.SkillDesc, this.effectDelayTime, this.damageValue);
        }
    }

    public override void UseSkill_Func()
    {
        base.UseSkill_Func();


    }
    protected override SkillType GetSkillType_Func()
    {
        return SkillType.Monster_Third_DeathRing;
    }
}