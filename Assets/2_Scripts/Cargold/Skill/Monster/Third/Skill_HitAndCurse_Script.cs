﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_HitAndCurse_Script : Skill_Script
{
    public float damageValue;
    public float poisonValue;
    public float posionLastTime;

    public override string SkillDesc
    {
        get
        {
            return base.SkillDesc;
        }
    }

    public override void UseSkill_Func()
    {
        base.UseSkill_Func();


    }

    protected override SkillType GetSkillType_Func()
    {
        return SkillType.Monster_Third_HitAndCurse;
    }
}