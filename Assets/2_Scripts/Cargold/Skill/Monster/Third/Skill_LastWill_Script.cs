﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_LastWill_Script : Skill_Script
{
    public float damageValue;

    public override string SkillDesc
    {
        get
        {
            return string.Format(base.SkillDesc, this.damageValue);
        }
    }

    public override void UseSkill_Func()
    {
        base.UseSkill_Func();

        Character_Cargold[] _charClassArr = null;

        for (int i = 0; i < _charClassArr.Length; i++)
        {
            _charClassArr[i].Damaged_Func(damageValue);
        }
    }

    protected override SkillType GetSkillType_Func()
    {
        return SkillType.Monster_Third_LastWill;
    }
}