﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_ResistByDamage_Script : Skill_Script
{
    public float lastTime;

    public override string SkillDesc
    {
        get
        {
            return string.Format(base.SkillDesc, lastTime);
        }
    }

    public override void UseSkill_Func()
    {
        base.UseSkill_Func();


    }

    protected override SkillType GetSkillType_Func()
    {
        return SkillType.Monster_Third_ResistByDamage;
    }
}