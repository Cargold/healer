﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalAttack_Monster_First_Script : NormalAttack_Script
{
    protected override SkillType GetSkillType_Func()
    {
        return SkillType.Monster_First_NormalAttack;
    }
}
