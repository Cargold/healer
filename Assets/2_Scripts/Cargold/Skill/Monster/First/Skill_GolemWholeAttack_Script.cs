﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_GolemWholeAttack_Script : Skill_Script
{
    public float damageValue;

    public override void UseSkill_Func()
    {
        base.UseSkill_Func();

        Character_Cargold[] _charClassArr = null;

        for (int i = 0; i < _charClassArr.Length; i++)
        {
            _charClassArr[i].Damaged_Func(damageValue);
        }
    }

    protected override SkillType GetSkillType_Func()
    {
        return SkillType.Monster_First_WholeAttack;
    }
}