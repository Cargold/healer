﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_GolemAttack2_Script : NormalAttack_Script
{
    protected override SkillType GetSkillType_Func()
    {
        return SkillType.Monster_First_NormalAttack2;
    }
}