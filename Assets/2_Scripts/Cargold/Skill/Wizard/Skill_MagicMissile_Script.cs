﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_MagicMissile_Script : Skill_Script
{
    public float missileDamage;

    public override void UseSkill_Func()
    {
        base.UseSkill_Func();

        CharacterSystem_Manager.Instance.monster.Damaged_Func(this.missileDamage);
    }

    protected override SkillType GetSkillType_Func()
    {
        return SkillType.Wizard_MagicMissile;
    }
}