﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class NormalAttack_Script : Skill_Script
{
    public float damageValue;

    public override string SkillDesc
    {
        get
        {
            return string.Format(base.SkillDesc, damageValue);
        }
    }
    public override void UseSkill_Func()
    {
        base.UseSkill_Func();

        Character_Cargold _targetCharClass = CharacterSystem_Manager.Instance.monster;

        _targetCharClass.Damaged_Func(damageValue);
    }
}
