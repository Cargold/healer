﻿using Cargold.WhichOne;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageSystem_Manager : MonoBehaviour
{
    public static StageSystem_Manager Instance;

    public WhichOne<Character_Cargold> selectedCharacter;

    public SkillSlotGroup_Script skillSkillSlotGroupClass;
    public SkillSelect_Script skillSelectClass;
    public GameResult_Script resultClass;

    public static Character_Cargold HealerCharClass { get { return CharacterSystem_Manager.Instance.healer; } } // 캐릭터 매니저에게서 healerCharClass 가져오기
    public Character_Cargold TagetCharClass { get { return selectedCharacter.GetWhichOne_Func(); } }

    public void Init_Func(int _stageID)
    {
        Instance = this;

        selectedCharacter = new WhichOne<Character_Cargold>();

        skillSkillSlotGroupClass.Init_Func();
        skillSelectClass.Init_Func();
        resultClass.Init_Func();
    }

    public void GameStart_Func()
    {
        CharacterSystem_Manager.Instance.GameStart();
    }

    public void SelectedCharacter_Func(Character_Cargold _charClass)
    {
        // 선택된 캐릭터가 있다면...
        if (selectedCharacter.HasWhichOne_Func() == true)
        {
            Character_Cargold _selectedCharClass = selectedCharacter.GetWhichOne_Func();

            // 선택되어 있는 캐릭터와, 새로 선택된 캐릭터가 동일한 캐릭터가 않다면...
            if (_selectedCharClass != _charClass)
            {
                selectedCharacter.Selected_Func(_charClass);
            }
            else
            {
                selectedCharacter.SelectCancel_Func();
            }
        }
        // 선택된 캐릭터가 없다면...
        else
        {
            selectedCharacter.Selected_Func(_charClass);
        }
    }

    public void SelectedSkill_Func(SkillSlotElem_Script _skillSlotClass)
    {
        if (selectedCharacter.HasWhichOne_Func() == true)
        {
            float _manaCost = _skillSlotClass.SkillClass.ManaCost;

            if (HealerCharClass.HasMana_Func(_manaCost) == true)
            {
                HealerCharClass.UseSkill_Func(_manaCost);
            }
            else
            {

            }
        }
        else
        {
            NotifySelectCharacter_Func();
        }
    }

    private void NotifySelectCharacter_Func()
    {

    }

    void UseSkill_Func()
    {

    }

    public void CallBtn_SelectBg_Func()
    {
        if (selectedCharacter.HasWhichOne_Func() == true)
        {
            selectedCharacter.SelectCancel_Func();

            skillSkillSlotGroupClass.SkillSelectedCancel_Func();
        }
        else
        {

        }
    }

    public void MonsterDie_Func()
    {
        GameSystem_Manager.Instance.StageClear_Func();

        resultClass.GameResult_Func(ResultType.Victory);
    }

    public void HeroDieAll_Func()
    {
        resultClass.GameResult_Func(ResultType.Defeat);
    }

    public void GameClear_Func()
    {

    }
}
