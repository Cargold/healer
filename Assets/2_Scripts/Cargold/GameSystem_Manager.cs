﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameSystem_Manager : Cargold_Singleton<GameSystem_Manager>
{
    [SerializeField] private GameState gameState;
    [SerializeField] private int stageID;
    
    public Button gameStartBtn;

    public GameState GameState { get { return gameState; } }
    public int StageID { get { return stageID; } }

    public int playerStageUnlock;

    private void Awake()
    {
        gameStartBtn.enabled = true;

        GameSystem_Manager.instance = this;
        DontDestroyOnLoad(this.gameObject);

        DataBase_Manager.Generate_Func();
        DataBase_Manager.Instance.Init_Func();

        PlayerSystem_Manager.Generate_Func();
        PlayerSystem_Manager.Instance.Init_Func();

        playerStageUnlock = 0;

        this.gameState = GameState.Title;
    }
    
    public void CallBtn_StageSelect_Func()
    {
        gameStartBtn.enabled = false;

        PlayerSystem_Manager.instance.SetHealerInitSkill_Func();

        this.GoToStageSelect_Func();
    }

    public void GoToStageSelect_Func()
    {
        StartCoroutine(StageSelectSceneLoad_Cor());
    }

    IEnumerator StageSelectSceneLoad_Cor()
    {
        this.gameState = GameState.StageSelect;

        AsyncOperation _async = SceneManager.LoadSceneAsync("StageSelect");

        while (_async.isDone == false)
        {
            yield return null;
        }

        FindObjectOfType<StageSelect_Manager>().Init_Func();
    }

    public void StageSelect_Func(int _stageID)
    {
        this.stageID = _stageID;

        StartCoroutine(GameSceneLoad_Cor());
    }

    IEnumerator GameSceneLoad_Cor()
    {
        this.gameState = GameState.PrepareToBattle;

        AsyncOperation _async = SceneManager.LoadSceneAsync("Game");

        while(_async.isDone == false)
        {
            yield return null;
        }

        FindObjectOfType<StageSystem_Manager>().Init_Func(this.stageID);

        this.gameState = GameState.StagePlay;
    }

    public override IEnumerator Init_Cor()
    {
        yield break;
    }

    public void StageClear_Func()
    {

    }
}

public enum GameState
{
    None = -1,
    Title,
    StageSelect,
    PrepareToBattle,
    StagePlay,
}