﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold.WhichOne;

public class SkillSelect_Script : MonoBehaviour
{
    public ToolTip_Script tooltipClass;
    public WhichOne<SkillSelectElem_Script> elemWhichOne;

    public List<SkillSelectElem_Script> equipElemClassList;
    public List<SkillSelectElem_Script> reserveElemClassList;

    public GameObject skillSelectElemObj;

    public Transform equipSkillGroupTrf;
    public Transform reserveSkillGroupTrf;

    public void Init_Func()
    {
        tooltipClass.Init_Func();

        elemWhichOne = new WhichOne<SkillSelectElem_Script>();

        equipElemClassList = new List<SkillSelectElem_Script>();
        Skill_Script[] _equipSkillClassArr = PlayerSystem_Manager.Instance.EquipSkillClassArr;
        for (int i = 0; i < _equipSkillClassArr.Length; i++)
        {
            SkillSelectElem_Script _elemClass = GenerateElem_Func(this.equipSkillGroupTrf);

            _elemClass.SetSkill_Func(SkillSelectElem_Script.ElemState.Equip, _equipSkillClassArr[i]);

            equipElemClassList.AddNewItem_Func(_elemClass);
        }

        reserveElemClassList = new List<SkillSelectElem_Script>();
        Skill_Script[] _reserveSkillClassArr = PlayerSystem_Manager.Instance.ReserveSkillClassArr;

        if(_reserveSkillClassArr == null)
        {

        }
        else
        {
            for (int i = 0; i < _reserveSkillClassArr.Length; i++)
            {
                SkillSelectElem_Script _elemClass = GenerateElem_Func(this.reserveSkillGroupTrf);

                _elemClass.SetSkill_Func(SkillSelectElem_Script.ElemState.Reserve, _reserveSkillClassArr[i]);

                reserveElemClassList.AddNewItem_Func(_elemClass);
            }
        }
        
        this.tooltipClass.SetData_Func(equipElemClassList[0].SkillClass);
    }
    
    private SkillSelectElem_Script GenerateElem_Func(Transform _parentTrf)
    {
        GameObject _skillSelectElemObj = GameObject.Instantiate(skillSelectElemObj, _parentTrf);
        SkillSelectElem_Script _elemClass = _skillSelectElemObj.GetComponent<SkillSelectElem_Script>();
        _elemClass.Init_Func(this);
        return _elemClass;
    }

    public void SelectElem_Func(SkillSelectElem_Script _elemClass)
    {
        this.elemWhichOne.Selected_Func(_elemClass);

        this.tooltipClass.SetData_Func(_elemClass.SkillClass);

        SkillSelectElem_Script.ElemState _elemState = _elemClass.elemState;

        if(_elemState == SkillSelectElem_Script.ElemState.Equip)
        {
            if(1 < equipElemClassList.Count)
            {
                equipElemClassList.Remove_Func(_elemClass);
                reserveElemClassList.AddNewItem_Func(_elemClass);

                _elemClass.SetState_Func(SkillSelectElem_Script.ElemState.Reserve);
                _elemClass.transform.SetParent(reserveSkillGroupTrf);
            }
            // 장착된 스킬 엘렘이 1개 이하라면...
            else
            {
                
            }
        }
        else if (_elemState == SkillSelectElem_Script.ElemState.Reserve)
        {
            if(equipElemClassList.Count < 3)
            {
                reserveElemClassList.Remove_Func(_elemClass);
                equipElemClassList.AddNewItem_Func(_elemClass);

                _elemClass.SetState_Func(SkillSelectElem_Script.ElemState.Equip);
                _elemClass.transform.SetParent(equipSkillGroupTrf);
            }
            // 스킬 슬롯이 모두 찼다면...
            else
            {

            }
        }
        else
        {
            throw new System.Exception("_elemState 예외처리 : " + _elemState);
        }
    }

    public void CallBtn_Start_Func()
    {
        Skill_Script[] _skillClassArr_Equip2 = new Skill_Script[this.equipElemClassList.Count];
        for (int i = 0; i < _skillClassArr_Equip2.Length; i++)
        {
            _skillClassArr_Equip2[i] = this.equipElemClassList[i].SkillClass;
        }
        PlayerSystem_Manager.Instance.SetSkill_Equip_Func(_skillClassArr_Equip2);

        Skill_Script[] _skillClassArr_Reserve = new Skill_Script[this.reserveElemClassList.Count];
        for (int i = 0; i < _skillClassArr_Reserve.Length; i++)
        {
            _skillClassArr_Reserve[i] = this.reserveElemClassList[i].SkillClass;
        }
        PlayerSystem_Manager.Instance.SetSkill_Reserve_Func(_skillClassArr_Reserve);

        StageSystem_Manager.Instance.GameStart_Func();

        this.gameObject.SetActive(false);
    }
}