﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cargold.WhichOne;

public class SkillSelectElem_Script : MonoBehaviour, IWhichOne
{
    private SkillSelect_Script selectClass;
    private Skill_Script skillClass;

    public ElemState elemState;
    public Image bgImg;
    public Image iconImg;

    public Skill_Script SkillClass { get { return skillClass; } }

    public void Init_Func(SkillSelect_Script _selectClass)
    {
        this.selectClass = _selectClass;

        this.elemState = ElemState.None;

        this.skillClass = null;
    }

    public void SetSkill_Func(ElemState _elemState, Skill_Script _skillClass)
    {
        this.elemState = _elemState;

        this.skillClass = _skillClass;

        //if (_skillClass.SkillIconSprite == null)


        Debug.Log("Icon 없음 : " + _skillClass.SkillIconSprite);
        iconImg.SetNativeSize_Func(_skillClass.SkillIconSprite);
    }

    public void CallBtn_Func()
    {
        if(this.skillClass == null)
        {

        }
        else
        {
            selectClass.SelectElem_Func(this);
        }
    }

    public void Selected_Func(bool _repeat = false)
    {

    }

    public void SelectCancel_Func()
    {

    }

    public void SetState_Func(ElemState _elemState)
    {
        this.elemState = _elemState;
    }

    public enum ElemState
    {
        None = -1,
        Equip,
        Reserve,
    }
}