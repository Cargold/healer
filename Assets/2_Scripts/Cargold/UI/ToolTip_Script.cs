﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolTip_Script : MonoBehaviour
{
    public Image iconImg;
    public Text nameTxt;
    public Text manaCostTxt;
    public Text castingTimeTxt;
    public Text CooltimeTxt;
    public Text descTxt;

    public void Init_Func()
    {
        iconImg.sprite = null;

        nameTxt.text = "Name";
        manaCostTxt.text = "마나 소모량 : ";
        castingTimeTxt.text = "시전시간 : ";
        CooltimeTxt.text = "쿨타임 : ";
        descTxt.text = "Desc";
    }

    public void SetData_Func(Skill_Script _skillClass)
    {
        iconImg.SetNativeSize_Func(_skillClass.SkillIconSprite);

        nameTxt.text = _skillClass.SkillName;
        manaCostTxt.text = "마나 소모량 : " + (int)_skillClass.ManaCost;
        castingTimeTxt.text = "시전시간 : " + (int)_skillClass.CastingTime;
        CooltimeTxt.text = "쿨타임 : " + (int)_skillClass.CoolTime;
        descTxt.text = _skillClass.SkillDesc;
    }
}
