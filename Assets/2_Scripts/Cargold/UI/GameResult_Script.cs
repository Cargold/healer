﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameResult_Script : MonoBehaviour
{
    public GameObject victoryObj;
    public GameObject defeatObj;

    public void Init_Func()
    {
        victoryObj.SetActive(false);
        defeatObj.SetActive(false);
    }

    public void GameResult_Func(ResultType _resultType)
    {
        if(_resultType == ResultType.Victory)
        {
            victoryObj.SetActive(true);
        }
        else if (_resultType == ResultType.Defeat)
        {
            defeatObj.SetActive(true);
        }
        else
        {
            throw new System.Exception("_resultType 예외처리 : " + _resultType);
        }
    }

    public void CallBtn_Func()
    {
        GameSystem_Manager.Instance.GoToStageSelect_Func();
    }
}

public enum ResultType
{
    None = -1,
    Victory,
    Defeat,
}