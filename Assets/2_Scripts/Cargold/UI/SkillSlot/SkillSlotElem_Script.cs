﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold.WhichOne;

public class SkillSlotElem_Script : MonoBehaviour, IWhichOne
{
    private SkillSlotGroup_Script groupClass;
    [SerializeField] private Skill_Script skillClass;
    public SkillSlotElem_View_Script viewClass;
    private bool isSelected;

    public bool HasSkill { get { return skillClass != null ? true : false; } }
    public bool IsSelected { get { return isSelected; } }
    public Skill_Script SkillClass { get { return skillClass; } }

    public void Init_Func(SkillSlotGroup_Script _groupClass)
    {
        this.groupClass = _groupClass;
        viewClass.Init_Func();
    }

    public void SetSkill_Func(Skill_Script _skillClass)
    {
        this.skillClass = _skillClass;

        if(_skillClass == null)
        {
            viewClass.Lock_Func();
        }
        else
        {
            viewClass.Unlock_Func();
        }
    }

    public void CallBtn_Func()
    {
        groupClass.SelectedElem_Func(this);
    }

    public void SelectCancel_Func()
    {
        isSelected = false;

        viewClass.SelectCancel_Func();
    }

    public void Selected_Func(bool _repeat = false)
    {
        isSelected = true;

        viewClass.Selected_Func();
    }
}
