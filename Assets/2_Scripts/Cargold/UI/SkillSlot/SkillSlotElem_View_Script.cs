﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillSlotElem_View_Script : MonoBehaviour
{
    public Image borderImg;

    public void Init_Func()
    {

    }

    public void Lock_Func()
    {

    }

    public void Unlock_Func()
    {

    }

    public void Selected_Func()
    {
        borderImg.color = Color.red;
    }
    public void SelectCancel_Func()
    {
        borderImg.color = Color.white;
    }
}
