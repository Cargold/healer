﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold.WhichOne;

public class SkillSlotGroup_Script : MonoBehaviour
{
    public SkillSlotElem_Script[] elemArr;
    public WhichOne<SkillSlotElem_Script> elemWhichOne;

    public void Init_Func()
    {
        for (int i = 0; i < elemArr.Length; i++)
        {
            elemArr[i].Init_Func(this);
        }

        elemWhichOne = new WhichOne<SkillSlotElem_Script>();
    }

    public void GameStart_Func()
    {
        Skill_Script[] skillClassArr = PlayerSystem_Manager.Instance.EquipSkillClassArr;

        for (int i = 0; i < skillClassArr.Length; i++)
        {
            if (i < skillClassArr.Length)
            {
                elemArr[i].SetSkill_Func(skillClassArr[i]);
            }
            else
            {
                elemArr[i].SetSkill_Func(null);
            }
        }
    }

    public void SetSkill_Func(Skill_Script[] skillClassArr, bool _isInit)
    {
        if(skillClassArr.Length <= 3)
        {
            
        }
        else
        {
            throw new System.Exception("스킬 슬롯에 넣으려는 스킬의 개수 초과 : " + skillClassArr.Length);
        }
    }

    public void SelectedElem_Func(SkillSlotElem_Script _elemClass)
    {
        elemWhichOne.Selected_Func(_elemClass);

        Skill_Script skillClass = _elemClass.SkillClass;
        float _manaCost = skillClass.ManaCost;

        Character_Cargold _healerCharClass = StageSystem_Manager.HealerCharClass;

        if(_healerCharClass.HasMana_Func(_manaCost) == true)
        {
            StageSystem_Manager.Instance.SelectedSkill_Func(_elemClass);
        }
        else
        {
            // Skill Elem Select Fail
        }
    }

    public void SkillSelectedCancel_Func()
    {
        if (elemWhichOne.HasWhichOne_Func() == true)
        {
            elemWhichOne.SelectCancel_Func();
        }
        else
        {

        }
    }
}
